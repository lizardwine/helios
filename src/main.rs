#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(helios::test_runner)]
#![reexport_test_harness_main = "test_main"]


extern crate alloc;

use alloc::string::String;
use helios::println;
use helios::vga_buffer::{change_color_foreground, Color};
use helios::task::{executor::Executor, keyboard, Task};
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    use helios::allocator;
    use helios::memory::{self, BootInfoFrameAllocator};
    use x86_64::VirtAddr;
    change_color_foreground(Color::Cyan);
    println!("Welcome to HeliOS.");
    println!("This is the v{} kernel's version.", env!("CARGO_PKG_VERSION"));
    println!("Starting HeliOS...");
    helios::init();
    

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_map) };

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    #[cfg(test)]
    test_main();

    let mut executor = Executor::new();
    change_color_foreground(Color::Yellow);
    executor.spawn(Task::new(String::from("keyboard listener"), keyboard::print_keypresses()));
    change_color_foreground(Color::Cyan);
    println!("HeliOS started.");
    executor.run();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    helios::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    helios::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
