# Introduction

## Contributing

You can help out a high-profile project that is used in a lot of places on the web. No big commitment required, if all you do is review a single [Merge Request](https://gitlab.com/lizardwine/helios/-/merge_requests), you are a maintainer.

### Your First Contribution

- review a [Merge Request](https://gitlab.com/lizardwine/helios/-/merge_requests)
- fix an [Issue](https://gitlab.com/lizardwine/helios/-/issues)
- update the [documentation](https://gitlab.com/lizardwine/helios/-/wikis)
- make a feature proposal
- write a tutorial

### Guidelines.

>Following these guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your pull requests.

### What kind of contributions is HeliOS looking for?


> HeliOS is an open source project and we love to receive contributions from you — There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into HeliOS itself.

> Please, don't use the issue tracker for support questions. Check whether the #support channel on our [discord server](https://discord.gg/XEVJAY8Vbj) can help with your issue.

# Ground Rules

> Responsibilities
> * Ensure that your code is safe.
> * Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
> * Keep your pull requests short and to the point.
> * Follow [Code of Conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/)

# Your First Contribution

> Unsure where to begin contributing to HeliOS? You can start by looking through these beginner and help-wanted issues:
> * Beginner issues - issues which should only require a few lines of code, and a test or two.
> * Help wanted issues - issues which should be a bit more involved than beginner issues.
> * Both issue lists are sorted by total number of comments. While not perfect, number of comments is a reasonable proxy for impact a given change will have.

### First time contributing to open source projects?

> Working on your first Pull / Merge Request? You can learn how from this *free* series, [How to write the perfect pull request](https://github.blog/2015-01-21-how-to-write-the-perfect-pull-request/).

>At this point, you're ready to make your changes! Feel free to ask for help; everyone is a beginner at first 😸
>
>If a maintainer asks you to "rebase" your PR, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge.

# Getting started
### Give them a quick walkthrough of how to submit a contribution.
>For any contribution you must run the tests with `cargo test`

How you write this is up to you, but some things you may want to include:



>For something that is bigger than a one or two line fix:

>1. Create your own fork of the code
>2. Do the changes in your fork
>3. Create and run the tests
>4. Create a pull request


### Small or "obvious" fixes.

>As a rule of thumb, changes are obvious fixes if they do not introduce any new functionality or creative thinking. As long as the change does not affect functionality, some likely examples include the following:
>* Spelling / grammar fixes
>* Typo correction, white space and formatting changes
>* Comment clean up
>* Bug fixes that change default return values or error codes stored in constants
>* Adding logging messages or debugging output
>* Changes to ‘metadata’ files like Gemfile, .gitignore, build scripts, etc.
>* Moving source files from one directory or package to another

# How to report a bug

> If you find a security vulnerability, do NOT open an issue. Email lizardwine@hotmail.com or open a ticket in our [discord server](https://discord.gg/XEVJAY8Vbj) instead.


> Any security issues should be submitted directly to lizardwine@hotmail.com with the subject line "Security Issue" or open a ticket in our [discord server](https://discord.gg/XEVJAY8Vbj).
> In order to determine whether you are dealing with a security issue, ask yourself these two questions:
> * Can I access something that's not mine, or something I shouldn't have access to?
> * Can I disable something for other people?
>
> If the answer to either of those two questions are "yes", then you're probably dealing with a security issue. Note that even if you answer "no" to both questions, you may still be dealing with a security issue, so if you're unsure, just email us at lizardwine@hotmail.com with the subject line "Probably security Issue" or open a ticket in our [discord server](https://discord.gg/XEVJAY8Vbj).

### Tell your contributors how to file a bug report.
You can even include a template so people can just copy-paste (again, less work for you).

> When filing an issue, make sure to answer these five questions:
>
> 1. if you are building from source, what version of rust are you using?
> 2. What version of HeliOS are you using?
> 3. What did you do?
> 4. What did you expect to see?
> 5. What did you see instead?

# How to suggest a feature or enhancement

> If you find yourself wishing for a feature that doesn't exist in HeliOS, you are probably not alone. There are bound to be others out there with similar needs. Open an issue on our issues list on Gitlab which describes the feature you would like to see, why you need it, and how it should work.

# Community

> You can chat with the core team on our [discord server](https://discord.gg/XEVJAY8Vbj).